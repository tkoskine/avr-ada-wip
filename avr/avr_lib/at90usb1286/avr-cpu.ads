---------------------------------------------------------------------------
--                                                                       --
-- The AVR-Ada Library is free software;  you can redistribute it and/or --
-- modify it under terms of the  GNU General Public License as published --
-- by  the  Free Software  Foundation;  either  version 2, or  (at  your --
-- option) any later version.  The AVR-Ada Library is distributed in the --
-- hope that it will be useful, but  WITHOUT ANY WARRANTY;  without even --
-- the  implied warranty of MERCHANTABILITY or FITNESS FOR A  PARTICULAR --
-- PURPOSE. See the GNU General Public License for more details.         --
--                                                                       --
-- As a special exception, if other files instantiate generics from this --
-- unit,  or  you  link  this  unit  with  other  files  to  produce  an --
-- executable   this  unit  does  not  by  itself  cause  the  resulting --
-- executable to  be  covered by the  GNU General  Public License.  This --
-- exception does  not  however  invalidate  any  other reasons why  the --
-- executable file might be covered by the GNU Public License.           --
---------------------------------------------------------------------------
--
--
-- CPU clock frequency control via prescaler register.
--
-- Note that final clock frequency depends on the used clock source
-- For example, currently the package expects 16MHz external clock
-- as found on Teensy++ 2.0 and many Arduino boards.

with Interfaces;

package AVR.CPU is
   CPU_16MHz  : constant := 16#00#;
   CPU_8MHz   : constant := 16#01#;
   CPU_4MHz   : constant := 16#02#;
   CPU_2MHz   : constant := 16#03#;
   CPU_1MHz   : constant := 16#04#;
   CPU_500khz : constant := 16#05#;
   CPU_250khz : constant := 16#06#;
   CPU_125khz : constant := 16#07#;
   CPU_62khz  : constant := 16#08#;

   procedure Set_Clock_Prescaler (Value : Interfaces.Unsigned_8);
end AVR.CPU;
